package com.deveducation.www;

import java.util.Random;

public class RandomMethods {


    Random random = new Random();


    public int showRandomNumber(){
        int result = random.nextInt();
        System.out.println(result);
        return result;
    }

    public void show10RandomNumbers(){
        for (int i = 0; i < 10; i++){
            System.out.print(random.nextInt() + " ");
        }
        System.out.println();
    }

    public void show10RandomNumbersFrom0To10(){
        for (int i = 0; i < 10; i++){
            System.out.print(random.nextInt(11) + " ");
        }
        System.out.println();
    }

    public void show10RandomNumbersFrom20To50(){
        int[] numbers = new int[31];
        int numberToFill = 20;
        for (int i = 0; i < numbers.length; i++){
            numbers[i] = numberToFill;
            numberToFill++;
        }
        for (int i = 0; i < 10; i++){
            System.out.print(numbers[random.nextInt(numbers.length)] + " ");
        }
        System.out.println();
    }

    public void show10RandomNumbersFromMinus10To10(){
        int[] numbers = new int[21];
        int numberToFill = -10;
        for (int i = 0; i < numbers.length; i++){
            numbers[i] = numberToFill;
            numberToFill++;
        }
        for (int i = 0; i < 10; i++){
            System.out.print(numbers[random.nextInt(numbers.length)] + " ");
        }
        System.out.println();
    }

    public void showFrom3To15OfRandomNumbersFromMinus10To35(){
        int[] amountNumbers = new int[13];
        int numberToFill1 = 3;
        for (int i = 0; i < amountNumbers.length; i++){
            amountNumbers[i] = numberToFill1;
            numberToFill1++;
        }
        int numsAmount = amountNumbers[random.nextInt(amountNumbers.length)];
        int[] numbers = new int[26];
        int numberToFill2 = 10;
        for (int i = 0; i < numbers.length; i++){
            numbers[i] = numberToFill2;
            numberToFill2++;
        }
        for (int i = 0; i < numsAmount; i++){
            System.out.print(numbers[random.nextInt(numbers.length)] + " ");
        }
        System.out.println();
    }
}
